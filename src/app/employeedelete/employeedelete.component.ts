import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { EmployeelistComponent } from '../employeelist/employeelist.component';

@Component({
  selector: 'app-employeedelete',
  templateUrl: './employeedelete.component.html',
  styleUrls: ['./employeedelete.component.css']
})
export class EmployeedeleteComponent implements OnInit {

  id: any;
  data: any=[];
  message: any;

  constructor(private service:EmployeeService) { }
  
  deleteEmployee(){
    this.service.delete(this.id).subscribe(res=>{
    console.log(res);
    });
    alert("successfully! deleted Records");
  }
  
  valuechange(){
    console.log(this.id);
    //this.service.getid(this.id).subscribe(res=> this.data=res);
  }

  ngOnInit() {
    this.id=this.service.getIdDelete();
    this.service.getid(this.id).subscribe(res=> this.data=res);
  }

}
