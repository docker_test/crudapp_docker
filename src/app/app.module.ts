import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { EmployeelistComponent } from './employeelist/employeelist.component';
import { EmployeeupdateComponent } from './employeeupdate/employeeupdate.component';
import { AddemployeelistComponent } from './addemployeelist/addemployeelist.component';
import { HttpClientModule } from '@angular/common/http';
import { EmployeedeleteComponent } from './employeedelete/employeedelete.component';

@NgModule({
  declarations: [
    AppComponent,
    EmployeelistComponent,
    EmployeeupdateComponent,
    AddemployeelistComponent,
    EmployeedeleteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
