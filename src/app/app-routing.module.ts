import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AddemployeelistComponent } from './addemployeelist/addemployeelist.component';
import { EmployeeupdateComponent} from './employeeupdate/employeeupdate.component';
import { EmployeelistComponent } from './employeelist/employeelist.component';
import { EmployeedeleteComponent } from './employeedelete/employeedelete.component';

export const routes: Routes = [
  { path: "add",component:AddemployeelistComponent},
  { path: "update", component:EmployeeupdateComponent},
  { path: "view", component:EmployeelistComponent},
  { path: "delete", component:EmployeedeleteComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }
