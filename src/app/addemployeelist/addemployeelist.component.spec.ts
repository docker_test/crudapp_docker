import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddemployeelistComponent } from './addemployeelist.component';

describe('AddemployeelistComponent', () => {
  let component: AddemployeelistComponent;
  let fixture: ComponentFixture<AddemployeelistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddemployeelistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddemployeelistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
